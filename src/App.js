import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import React, { Component } from "react";
import { key, YES } from "./constants";
import { APP_TITLE } from "./constants";

// Components from Utils
import Header from "./components/layout/Header";
import Sidebar from "./components/layout/sidebar/Sidebar";
import Footer from "./components/layout/Footer";

//comment
import Login from "./pages/login/Login";
import Main from "./pages/main/Main";
import Home from "./pages/home/Home";
import Assembly from "./pages/assembly/Assembly";
import MasterSingle from "./pages/masterSG/MasterSingle";
import DemoScanner from "./pages/demoScanner/DemoScanner";
import CreateMR from "./pages/createMR/CreateMR";
import Register from "./pages/register/register";

import KitUp from "./pages/kit_up/kit_up";
import ProductionReport from "./pages/dashboard/productionReport";
import BalanceStock_Kitup from "./pages/dashboard/balanceStock_Kitup";
import BalanceStock_Line from "./pages/dashboard/balanceStock_Line";
import ProductionReport_All from "./pages/dashboard/productionReport_All";
import SummarizeReport from "./pages/dashboard/summarizeReport";
import Target from "./pages/dashboard/target";

import ScanFG from "./pages/scan/scan_FG";

import Kitupreceived from "./pages/kitup_received/kitup_received";
import Kitupissue from "./pages/kitup_issue/kitup_issue";

const isLoggedIn = () => {
  //return true if === YES
  return localStorage.getItem(key.LOGIN_PASSED) === YES;
};
const SecuredRoute = ({ children }) => {
  if (isLoggedIn()) {
    return children;
  }
  return <Navigate to="/login" />
}

// const SecuredRoute = ({ component: Component, ...rest }) => (
//   <Route
//     {...rest}
//     element={isLoggedIn() === true ? <Component /> : <Navigate to="/login" />}
//   />
// );

const SecuredAdminRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    element={
      isLoggedIn() === true ? (
        localStorage.getItem(key.USER_LV) === "admin" ? (
          <Component />
        ) : (
          <Navigate to="/main" />
        )
      ) : (
        <Navigate to="/login" />
      )
    }
  />
);

class App extends Component {
  componentDidMount() { }

  redirectToLogin = () => {
    return <Navigate to="/login" />;
  };
  render() {
    document.title = APP_TITLE;

    return (
      <BrowserRouter>
        <div className="">
          {isLoggedIn() && <Header />}
          {isLoggedIn() && <Sidebar />}

          <Routes>
            <Route path="/login" element={<Login />} />
            <Route
              path="/main"
              element={
                <SecuredRoute>
                  <Main />
                </SecuredRoute>
              }
            />
            <Route
              path="/home"
              element={
                <SecuredRoute>
                  <Home />
                </SecuredRoute>
              }
            />
            <Route
              path="/assembly_process"
              element={
                <SecuredRoute>
                  <Assembly />
                </SecuredRoute>
              }
            />
            <Route
              path="/demo_scanner"
              element={
                <SecuredRoute>
                  <DemoScanner />
                </SecuredRoute>
              }
            />
            <Route
              path="/Kitup_received"
              element={
                <SecuredRoute>
                  <Kitupreceived />
                </SecuredRoute>
              }
            />
            <Route
              path="/Kitup_issue"
              element={
                <SecuredRoute>
                  <Kitupissue />
                </SecuredRoute>
              }
            />
            <Route path="/CreateMR" element={<SecuredRoute> <CreateMR /> </SecuredRoute>} />
            <Route path="/register" element={<Register />} />
            <Route path="/kit_up" element={<KitUp />} />
            <Route path="/scan_FG" element={<ScanFG />} />
            <Route path="/productionReport" element={<ProductionReport />} />
            <Route path="/balanceStock_Kitup" element={<BalanceStock_Kitup />} />
            <Route path="/balanceStock_Line" element={<BalanceStock_Line />} />
            <Route path="/productionReport_All" element={<ProductionReport_All />} />
            <Route path="/SummarizeReport" element={<SummarizeReport />} />
            <Route path="/master_target" element={
              <SecuredRoute> <Target /></SecuredRoute>} />

            <Route path="/master_single_part" element={
              <SecuredAdminRoute>
                <MasterSingle />
              </SecuredAdminRoute>
            } />
            <Route path="/" element={<this.redirectToLogin />} />
            <Route path="*" element={<this.redirectToLogin />} />
          </Routes>

          {isLoggedIn() && <Footer />}
        </div>
      </BrowserRouter>
    );
  }
}
export default App;
