import React from 'react';

const Base64Image = ({ base64Image, size }) => {
  const getImageData = (base64Image) => {
    const img = new Image();
    img.src = base64Image;
    return img;
  };

  const resizeImage = (imageData, size) => {
    const canvas = document.createElement('canvas');
    canvas.width = size.width;
    canvas.height = size.height;

    const context = canvas.getContext('2d');
    context.drawImage(imageData, 0, 0, size.width, size.height);

    const resizedImage = new Image();
    resizedImage.src = canvas.toDataURL();
    return resizedImage;
  };

  const imageData = getImageData(base64Image);
  const resizedImageData = resizeImage(imageData, size);

  return <img src={resizedImageData.src} alt={"ResizeImage"} />;
};

export default Base64Image;
