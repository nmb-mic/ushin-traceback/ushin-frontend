import React, { Component } from 'react'

class TagScanner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tagScan: props.text 
      }
    }
  render() {
    return (
        <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
          <div className="card">
              <div className="card-body text-center">
                <h2>Assembly : Record</h2>
                <div className="row d-flex flex-column">
                  <div className="col">
                    <button className="btn btn-success btn-lg text-center mb-3 w-50">
                      Main
                    </button>
                  </div>
                  <div className="col">
                    <button className="btn btn-success btn-lg text-center mb-3 w-50">
                        Sub SS
                    </button>
                  </div>
                  <div className="col">
                    <button className="btn btn-success btn-lg text-center mb-3 w-50">
                      Sub S
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default TagScanner;
