import * as React from "react";
import { Link } from "react-router-dom";
// export const MenuItems =() => {

//   const menuListItems = [
//     { label: "Card1"},
//     { label: "Card2"},
//     { label: "Card3"},
//   ];
//   return (
//     <>
//       {menuListItems.map((item) => (
//         <div key={item.label} className="col-lg-3 col-6">
//           <div className="small-box bg-success">
//             <div className="inner">
//               <h3>{item.label}</h3>
//             </div>
//           </div>
//         </div>
//       ))}
//     </>
//   );
// };

export const MenuAdminItems = () => {
  const menuListItems = [
    { label: "Card1A" },
    { label: "Card2A" },
    { label: "Card3A" },
    { label: "Card4A" },
  ];
  return (
    <>
      {menuListItems.map((item) => (
        <div key={item.label} className="col-lg-3 col-6">
          <div className="small-box bg-success">
            <div className="inner">
              <h3>{item.label}</h3>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export const MenuItems = () => {
  return (
    <div className="container-fluid">
      <section>
        <div className="row">
          <div className="col-lg-3 col-6">
            <div className="small-box bg-info" style={{ height: "160px" }}>
              <div className="inner" style={{ color: "black" }}>
                <h3>Production result</h3>
              </div>
              <div className="icon" style={{ height: "40px" }}>
                <i className="ion ion-pie-graph" />
              </div>
              <Link
                to="/productionReport"
                className="small-box-footer"
                style={{ height: "50px", color: "white" }}
              >
                <a style={{ color: "white" }}>
                  More info <i className="fas fa-arrow-circle-right" />
                </a>
              </Link>
            </div>
          </div>

          <div className=" col-lg-3 col-6">
            <div className="small-box bg-danger" style={{ height: "160px" }}>
              <div className="inner" style={{ color: "black" }}>
                <h3>Summarize Output </h3> 
              </div>
              <div className="icon" style={{ height: "40px" }}>
                <i className="ion ion-connection-bars" />
              </div>
              <Link
                to="/SummarizeReport"
                className="small-box-footer"
                style={{ height: "50px", color: "white" }}
              >
                <a style={{ color: "white" }}>
                  More info <i className="fas fa-arrow-circle-right" />
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-3 col-6">
            <div className="small-box bg-success" style={{ height: "160px" }}>
              <div className="inner" style={{ color: "black" }}>
                <h3>Kitup Stock</h3>
              </div>
              <div className="icon" style={{ height: "40px" }}>
                <i className="ion ion-ios-box-outline" />
              </div>
              <Link
                to="/balanceStock_Kitup"
                className="small-box-footer"
                style={{ height: "50px", color: "white" }}
              >
                <a style={{ color: "white" }}>
                  More info <i className="fas fa-arrow-circle-right" />
                </a>
              </Link>
            </div>
          </div>
          <div className=" col-lg-3 col-6">
            <div className="small-box bg-warning" style={{ height: "160px" }}>
              <div className="inner">
                <h3>Line stock</h3>
              </div>
              <div className="icon" style={{ height: "40px" }}>
                <i className="ion ion-arrow-graph-down-left" />
              </div>
              <Link
                to="/balanceStock_Line"
                className="small-box-footer"
                style={{ height: "50px", color: "white" }}
              >
                <a style={{ color: "white" }}>
                  More info <i className="fas fa-arrow-circle-right" />
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="row"></div>
      </section>
    </div>
  );
};
