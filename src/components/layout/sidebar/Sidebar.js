import React from "react";
import { Link } from "react-router-dom";
import { key } from "../../../constants";
//import Subsidebar from "./sidebar2";

function Sidebar() {
  return (
    <aside className="main-sidebar sidebar-light-primary elevation-4 sidebar-no-expand">
      {/* Brand Logo */}
      <div className="brand-link">
        <span
          className="brand-text font-weight-dark"
          style={{
            marginBottom: "0",
           // fontWeight: 600,
            fontSize: "1.5rem",
            color: "#03a9f4",
            fontWeight: "bold",
          }}
        >
          U-Shin company
        </span>
      </div>
      {/* Sidebar */}
      <div className="sidebar">
        {/* Sidebar Menu */}
        <nav className="mt-2">
          <ul
            className="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false"
          >
            {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/main" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Dashboard</p>
              </Link>
            </li>
            {/* <li className="nav-item">
              <Link to="/assembly_process" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Assembly Process</p>
              </Link>
            </li> */}
            {/* <li className="nav-item">
              <Link to="/demo_scanner" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Demo Scanner</p>
              </Link>
            </li> */}
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/CreateMR" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Create M/R</p>
              </Link>
            </li>
            {/* <li className="nav-item"  style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/kit_up" className="nav-link">
                <i className="nav-icon fas fa-th"/>
                <p>Scan kit up</p>
              </Link>
            </li> */}
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/Kitup_received" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Kitup receive</p>
              </Link>
            </li>
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/Kitup_issue" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Kitup issue</p>
              </Link>
            </li>
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/scan_FG" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Scan F/G</p>
              </Link>
            </li>
            <li className="nav-item" style={{fontSize:"1.5rem", fontWeight: "bold", fontFamily: "Sans-Serif"}}>
              <Link to="/master_target" className="nav-link">
                <i className="nav-icon fas fa-th" />
                <p>Master Target</p>
              </Link>
            </li>




            {/* <Subsidebar /> */}

            {localStorage.getItem(key.USER_LV) === "admin" ? (
              <li className="nav-item">
                <Link to="/master_single_part" className="nav-link">
                  <i className="nav-icon fas fa-th" />
                  <p>Add Master Part</p>
                </Link>
              </li>
            ) : (
              <></>
            )}
          </ul>
        </nav>
        {/* /.sidebar-menu */}
      </div>
      {/* /.sidebar */}
    </aside>
  );
}
export default Sidebar;
