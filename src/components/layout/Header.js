import React, { Component } from "react";
import { Link } from "react-router-dom";
import { key } from "../../constants";
import { LogoutButton } from "./LogoutButton";

class Header extends Component {
  render() {
    return (
      <nav
        className="main-header navbar navbar-expand"
        style={{ backgroundColor: "rgba(60, 149, 255, 0.9)" }}
      >
        {/* Left navbar links */}
        <ul className="navbar-nav">
          <li className="nav-item">
            <div className="nav-link" data-widget="pushmenu" role="button">
              <i className="fas fa-bars" />
            </div>
            {/* <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a> */}
          </li>
          <li className="nav-item d-none d-sm-inline-block">
            <Link to="/main" className="nav-link text-dark">
              Home
            </Link>
          </li>
        </ul>
        {/* Right navbar links */}
        <ul className="navbar-nav ml-auto">
          <li className="nave-item">
            <div className="nav-link">
              <span style={{ color: "lightyellow" }}>
                EMP: {localStorage.getItem(key.USER_NAME)}(
                {localStorage.getItem(key.USER_EMP)})
              </span>
            </div>
          </li>
          {/* Navbar Logout */}
          {/* <li className="nav-item">
            <div
              className="nav-link"
              role="button"
              onClick={() => {
                this.props.history.navigate("/login");
                localStorage.removeItem(key.LOGIN_PASSED);
                localStorage.removeItem(key.USER_NAME);
                localStorage.removeItem(key.USER_LV);
                localStorage.removeItem(key.USER_EMP);
                this.props.appReducer.app.forceUpdate();
              }}
            >
              <b>Logout</b>
            </div>
          </li> */}
          <LogoutButton />
        </ul>
      </nav>
    );
  }
}

export default Header;
