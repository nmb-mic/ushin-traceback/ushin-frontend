import React, { Component } from "react";
import Swal from "sweetalert2";
import { server } from "../../constants/index";
import { httpClient } from "../../utils/HttpClient";
import moment from "moment";
//import moment from "moment/min/moment-with-locales";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";

export default class Kitupreceived extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MO_list: "",
      scanList: "",
      result: "",
      process: "SUB2",
      showRender: "none",
      line: "",
      data: null,
      start_date: moment().format("YYYY-MM-DD"),
      end_date: moment().format("YYYY-MM-DD"),
      clear_state: [],
      time: this.props.time,
      seconds: "1200",
    };
  }

  line_KeyPress = () => {
    this.inputline.focus();
  };

  insertMO = async () => {
    let insert_result = await httpClient.post(server.INSERT_KIT_RECEIVED_URL, {
      mo_date: this.state.MO_list[0],
      mo_no: this.state.MO_list[1],
      process: this.state.process,
      line: this.state.line,
      child_part_no: this.state.scanList[0],
      lotno: this.state.scanList[1],
      qty: this.state.scanList[2],
    });

    if (insert_result.data.api_result === "OK") {
      await Swal.fire({
        icon: "success",
        title: "บันทึกเรียบร้อย",
        text: "Record OK !!!",
        showConfirmButton: false,
        timer: 1000,
      });
      await this.getData();
      window.location.replace("../kitup_received");
    } else {
      await Swal.fire({
        icon: "error",
        title: "บันทึกซ้ำ",
        text: "Duplicate data",
        showConfirmButton: false,
        timer: 1000,
      });
      window.location.replace("../kitup_received");
    }
  };

  MO_KeyPress = async (event) => {
    if (event.key === "Enter") {
      await this.inputMO.focus();
    }
  };
  part_KeyPress = async (event) => {
    if (event.key === "Enter") {
      await this.input_part.focus();
    }
  };

  componentDidMount = async () => {
    this.getData();
    this.timer = setInterval(this.tick, 1000);
  };

  getData = async () => {
    const result = await httpClient.get(server.GET_MAT_KIT_RECEIVED_URL);
    this.setState({ data: result.data.result });
  };

  renderTableData() {
    if (this.state.data !== null) {
      console.log(this.state.data);
      return this.state.data.map((item) => (
        <tr>
          <td>{moment(item.createdAt).format("DD-MM-YYYY h:mm a")}</td>
          <td>{item.MO_DATE}</td>
          <td>{item.LINE}</td>
          <td>{item.CHILD_PART_NUMBER}</td>
          <td>{item.LOT_NO}</td>
          <td>{item.QTYS}</td>
        </tr>
      ));
    }
  }

  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="card" style={{ height: "220px" }}>
              <div
                className="card-header card-title text-bold"
                style={{
                  fontSize: "45px",
                  textAlign: "center",
                  height: "100px",
                }}
              >
                <p> KIT UP RECEIVED </p>
                <br />
              </div>
              <div className="card-body">
                <div className="row">
                  <div
                    className="col-2"
                    style={{
                      color: "#3495eb",
                      fontWeight: "bold",
                      textAlign: "center",
                      height: "50px",
                      fontSize: "25px",
                      width: "300px",
                    }}
                  >
                    <select
                      style={{
                        height: "50px",
                        fontSize: "1.5rem",
                        fontWeight: "bold",
                      }}
                      className="form-control"
                      name="process"
                      onChange={(e) => {
                        this.line_KeyPress();
                        this.setState({
                          process: e.target.value,
                        });
                      }}
                    >
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        hidden
                        defaultValue="SUB2"
                      >
                        SUB2
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        value="MAIN"
                      >
                        MAIN
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        value="SUB1"
                      >
                        SUB1
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        Value="SUB2"
                        selected
                      >
                        SUB2
                      </option>
                    </select>
                  </div>
                  <div className="col-2">
                    <input
                      style={{
                        color: "#3495eb",
                        fontWeight: "bold",
                        textAlign: "center",
                        height: "50px",
                        fontSize: "25px",
                        width: "250px",
                      }}
                      autoFocus
                      ref={(input) => {
                        this.inputline = input;
                      }}
                      type="text"
                      className="form-control"
                      onKeyPress={this.MO_KeyPress}
                      placeholder="Scan Line"
                      onInput={async (e) => {
                        try {
                          await this.setState({ line: e.target.value });
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-2">
                    <input
                      style={{
                        color: "#3495eb",
                        height: "50px",
                        fontWeight: "bold",
                        fontSize: "25px",
                        textAlign: "center",
                        width: "250px",
                      }}
                      ref={(input) => {
                        this.inputMO = input;
                      }}
                      type="text"
                      className="form-control"
                      // id="exampleInputEmail1"
                      onKeyPress={this.part_KeyPress}
                      placeholder="Scan M/O"
                      onInput={async (e) => {
                        try {
                          const event = e.target.value
                            .split(" ")
                            .filter((part) => part !== "");
                          console.log(event);

                          if (event[0]["0"] === "M") {
                            const eventNew = [event[0], event[1]];
                            await this.setState({ MO_list: eventNew });
                          } else {
                            await Swal.fire({
                              icon: "error",
                              title: "MO ไม่มีในระบบ",
                              text: "Wrong MO tag",
                              showConfirmButton: true,
                              timer: 3000,
                            });
                            window.location.replace("../kit_part");
                          }
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-4">
                    <input
                      style={{
                        color: "#3495eb",
                        fontWeight: "bold",
                        textAlign: "center",
                        fontSize: "25px",
                        width: "500px",
                        height: "50px",
                      }}
                      ref={(input) => {
                        this.input_part = input;
                      }}
                      type="text"
                      className="form-control"
                      // id="exampleInputEmail1"
                      placeholder="Material tag"
                      onInput={async (e) => {
                        try {
                          const event = e.target.value
                            .split(" ")
                            .filter((part) => part !== "");
                          console.log(event);

                          if (event[0]["0"] === "T") {
                            const eventNew = [
                              event[1],
                              event[2].substring(4),
                              event[4].substring(8, 12),
                            ];
                            await this.setState({ scanList: eventNew });
                          } else if (event[0]["0"] === "U") {
                            const eventNew = [
                              event[2].substring(3),
                              event[3].substring(4),
                              event[5].substring(9, 12),
                            ];
                            await this.setState({ scanList: eventNew });
                          } else if (event[0]["0"] === "K") {
                            const eventNew = [
                              event[0].substring(2),
                              event[1].substring(),
                              event[2].substring(12, 15),
                            ];
                            await this.setState({ scanList: eventNew });
                          } else {
                            await Swal.fire({
                              icon: "error",
                              title: "Tag ไม่มีในระบบ",
                              text: "Wrong tag",
                              showConfirmButton: true,
                              timer: 3000,
                            });
                            window.location.replace("../kitup_received");
                          }
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-1">
                    <button
                      style={{
                        marginBottom: "0",
                        fontSize: "1.5rem",
                        color: "white",
                        fontWeight: "bold",
                        width: "150px",
                      }}
                      className="btn btn-danger"
                      onClick={async (e) => {
                        try {
                          e.preventDefault();
                          this.insertMO();
                          if (this.state.result === "OK") {
                            await Swal.fire({
                              icon: "success",
                              title: "บันทึกเรียบร้อย",
                              text: "Record OK !!!",
                              showConfirmButton: false,
                              timer: 1000,
                            });
                          }
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    >
                      Add Part
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="row">
                <div className="table-responsive">
                  <MDBTable
                    responsive
                    style={{
                      textAlign: "center",
                      fontSize: "1.5rem",
                      width: "2220px",
                      marginLeft: "20px",
                      marginTop: "30px",
                    }}
                  >
                    <MDBTableHead
                      color="primary-color"
                      style={{ backgroundColor: "#54B4D3" }}
                      textblack
                    >
                      <tr>
                        <th> REGIST_DATE</th>
                        <th> MO_DATE</th>
                        <th> LINE</th>
                        <th> CHILD_PART_NUMBER</th>
                        <th> LOT_NO</th>
                        <th> QTY</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>{this.renderTableData()}</MDBTableBody>
                  </MDBTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
