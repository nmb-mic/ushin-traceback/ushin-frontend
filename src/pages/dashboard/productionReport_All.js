import React, { Component } from "react";
import { server } from "../../constants/index";
import { httpClient } from "../../utils/HttpClient";
// import Select from "react-select";

// ห้ามลบ SummarizeReport
import SummarizeReport from "./summarizeReport";

class ProductionReport extends Component {
  constructor(props) {
    super(props);
    this.tick = this.tick.bind(this);
   this.state = { seconds: props.seconds };
    this.state = {
      MO_list: "",
      scanList: "",
      result: "",
      process: "SUB2",
      line: "",
     seconds:"300000", //every 100 SEC
      FG_result_LINE:"",

      FG_result: "",
      FG_result_MO: "",
      FG_result_CN: "",

      productPN: "",
      moNumber: "",
    };
  }

  componentDidMount = async () => {
    await this.Line_Info();
    this.FG_result();
     
  
  };


  tick = async () => {
    if (this.state.seconds > 0) {
      this.setState({ seconds: this.state.seconds - 1 });
    } else {
      clearInterval(this.timer);
     window.location.reload();
     
    }
  }

  FG_result = async () => {
    try {
      let resultdata = await httpClient.get(server.GET_FG_SG_URL);
      var newResult = resultdata.data.result[0];

      var use_FG = Object.values(newResult)[2];
      this.setState({ FG_result: use_FG });
     this.timer = setInterval(this.tick);
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
    console.log(newResult);
    console.log(use_FG);
  };

  Line_Info = async () => {
    try {
      let resultdata = await httpClient.get(server.GET_FG_INFO_URL);
      var newResult = resultdata.data.result[0];

      var MO_FG = Object.values(newResult)[0];
      this.setState({ FG_result_MO: MO_FG });

      var CN_FG = Object.values(newResult)[2];
      this.setState({ FG_result_CN: CN_FG });

      var LINE_FG = Object.values(newResult)[4];
      this.setState({ FG_result_LINE: LINE_FG });

    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };


  render() {
    console.log(this.state.FG_result);
    return (
      <div className="wrapper">
        <div className="content-wrapper" style={{ minHeight: "1020px" }}>
          {/* <section className="content-header"> */}

          <div className="position-relative card-body"></div>

          <section className="content">
            <div className="mb-2 row" style={{ height: "70px" }}>
              <div className="col-12 col-lg-7">
                <div className="text-left col-auto">
                  <h1
                    style={{
                      marginBottom: "0",
                      fontWeight: 600,
                      fontSize: "3.5rem",
                      color: "gray",
                    }}
                  >
                    Customer PN : {this.state.FG_result_CN}
                  </h1>
                </div>
              </div>
            </div>
            <div className="container-fluid" style={{height:"550px"}}>
              <div className="card card-primary" style={{height:"380px"}}>
                <div className="card-header"></div>
                <h1>Line number : {this.state.FG_result_LINE} === MO number : {this.state.FG_result_MO} </h1>
               

                <div className="page-content">
                  <section style={{ height: "350px" }}>
                      {/* ห้ามลบ link หน้ากราฟ */}
                  <SummarizeReport />
                  </section>
                </div>

              
              </div>
              
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default ProductionReport;