import React, { Component } from 'react';
import { httpClient } from '../../utils/HttpClient';
import { server, key, OK } from '../../constants/index';
import Swal from 'sweetalert2';

class Target extends Component {

  constructor(props) {
    super(props)

    this.state = {
      Customer_PN: '',
      PROCESS: '',
      Target: '',
      updateBy: '',
      ItemNo: [],
      data_list : [],
      selected_item: '',
      updateBy: localStorage.getItem(key.USER_EMP),

    }
  }
  componentDidMount() {
    this.get_cusItem();
  }
  get_cusItem = async () => {
    let dataItem = await httpClient.post(server.master_cusItem)
    console.log(dataItem.data);
    await this.setState({
      ItemNo: dataItem.data.message,
      selected_item: dataItem.data.message[0].CUSTOMER_PN,
    })
    let datalist = await httpClient.post(server.Data_URL)
    console.log(datalist.data.result);
    await this.setState({ data_list: datalist.data.result })
  }
  renderTable() {
    // console.log(this.state.ItemNo);
    if (this.state.ItemNo != null) {
      return this.state.ItemNo.map((item) =>
        <option>{item.CUSTOMER_PN}</option>)
    }
  }
  renderTable1() {
    console.log(this.state.data_list);
    if (this.state.data_list != null) {
      return this.state.data_list.map((item) =>
        <tr>
          <th>{item.Customer_PN}</th>
          <th>{item.PROCESS}</th>
          <th>{item.Target}</th>
          <th>{item.updateBy}</th>
          <th> <button type="submit" className="btn btn-warning" > edit </button></th>
        </tr>
        )
    }
  }
  additem = async () => {
    let data = await httpClient.post(server.master_target_URL, {
      Customer_PN: this.state.selected_item,
      PROCESS: this.state.PROCESS,
      Target: this.state.Target,
      updateBy: localStorage.getItem(key.USER_EMP)
    })
    console.log(data);
    if (data.data.api_result == "OK") {
      await Swal.fire({
        icon: "success",
        title: "insert complete",
      })
    } else {
      await Swal.fire({
        icon: "error",
        title: "insert incomplete",
      })
    }
    window.location.replace("/master_target");
  }
  render() {
    return (
      <>
        <div className='content-wrapper'>
          <div className="content">
            <div className="container-fluid" style={{ fontFamily: 'sans-serif', textAlign: 'center' }} >
              <div className="card">
                <h1> Master  </h1>
                <div className="col-md-12"  >
                  <div className="card card-secondary">
                    <div className="card-header">
                      <h3 className="card-title"></h3>
                    </div>
                    <div className="card-body">
                      <div className="row"> 
                      <div className="col-md-1"> </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label> Customer No.</label>
                            <select type="text" className="form-control"
                              value={this.state.selected_item}
                              onChange={async (e) => {
                                await this.setState({ selected_item: e.target.value });
                              }}>
                              <option>option 1</option>
                              {this.renderTable()}
                            </select>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label> Process </label>
                            <select type="text" className="form-control"
                              value={this.state.PROCESS}
                              onChange={async (e) => {
                                await this.setState({ PROCESS: e.target.value });
                              }}>
                              <option> ---</option>
                              <option> SUB2</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label> Target </label>
                            <input type="text" className="form-control" placeholder="Qty"
                              value={this.state.Target}
                              onChange={async (e) => {
                                await this.setState({ Target: e.target.value });
                              }}>
                            </input>
                          </div>
                        </div>
                        <div className="col-md-1" style={{ padding: '7.5px' }}>
                          <div className="form-group">
                            <br />
                            <button type="submit" className="btn btn-primary"
                              onClick={(e) => {
                                e.preventDefault()
                                this.additem()
                              }}
                            >Add</button>
                          </div>
                        </div>
                      </div>
                      <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>CUSTOMER NUMBER</th>
                          <th>CPROCESS</th>
                          <th>Traget</th>
                          <th>Update By</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderTable1()}

                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </>
    );
  }
}

export default Target;

