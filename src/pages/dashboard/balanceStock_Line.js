import React, { Component } from "react";
import { server } from "../../constants/index";
import { httpClient } from "../../utils/HttpClient";
import moment from "moment";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";

export default class BalanceStock_Line extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MO_list: "",
      scanList: "",
      result: "",
      process: "SUB2",
      showRender: "none",
      line: "",
      data_KIT: null,
      data_LINE: null,
      start_date: moment().format("YYYY-MM-DD"),
      end_date: moment().format("YYYY-MM-DD"),
      clear_state: [],
      time: this.props.time,
      seconds: "1200",
    };
  }

  componentDidMount = async () => {
    this.getData();
    this.timer = setInterval(this.tick, 1000);
  };

  getData = async () => {
    const result_LINE = await httpClient.get(server.GET_MAT_LINE_BALANCE_URL);
    this.setState({ data_LINE: result_LINE.data.result });
  };

  renderTableData_LINE() {
    if (this.state.data_LINE !== null) {
      console.log(this.state.data_LINE);
      return this.state.data_LINE.map((item) => (
        <tr>
          <td>{item.newProcess}</td>
          <td>{item.newLINE}</td>
          <td>{item.newCHILD}</td>
          <td>{item.balanceQty}</td>
        </tr>
      ));
    }
  }

  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="card"></div>
            <div className="card" style={{ height: "100px" }}>
              <div
                className="card-header card-title text-bold"
                style={{
                  fontSize: "45px",
                  textAlign: "center",
                  height: "100px",
                }}
              >
                <p> LINE Balance stock </p>
                <br />
              </div>
            </div>
            <div className="card">
              <div className="row">
                <div className="table-responsive">
                  <MDBTable
                    responsive
                    style={{
                      textAlign: "center",
                      fontSize: "1.5rem",
                      width: "2220px",
                      marginLeft: "20px",
                      marginTop: "30px",
                    }}
                  >
                    <MDBTableHead
                      color="primary-color"
                      style={{ backgroundColor: "#54B4D3" }}
                      textblack
                    >
                      <tr>
                        <th> PROCESS</th>
                        <th> LINE</th>
                        <th> CHILD_PART_NUMBER</th>
                        <th> QTY</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>{this.renderTableData_LINE()}</MDBTableBody>
                  </MDBTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
