import React, { Component } from "react";
import { server } from "../../constants/index";
import { httpClient } from "../../utils/HttpClient";
import ReactApexChart from "react-apexcharts";
import moment from "moment";
//import { result, xorBy } from "lodash";

class SummarizeReport extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      output_options: {},
      output_series: [],
      output_options_day: {},
      output_series_day: [],

      series_stack: [],
      options_stack: {},
      series_hour: [],
      options_hour: {},
      mfg_date: moment("2023-08-01").add(-0, "days").format("YYYY-MM-DD"),
      line: "All",
      Customer: "All",
      stack: [],

      seriesMonth: [],
      optionsMonth: {},
    };
  }
  async componentDidMount() {
    await this.onClick_search();
  }
  onClick_search = async () => {
    await this.stack_daily();
    await this.stack_hour();
    await this.data_hour();
    await this.data_daily();
    await this.monthly_data();
  };

  output_hr = async () => {
    try {
      let result = await httpClient.post(server.OUTPUT_HR_URL, {
        mfg_date: this.state.mfg_date,
      });
      // console.log("line1", result.data.result);
      var label_output = [];
      var data_output_actual = [];
      for (let i = 0; i < result.data.result.length; i++) {
        label_output.push(result.data.result[i].hour);
        data_output_actual.push(result.data.result[i].QTYS);
      }
      await this.setState({
        output_series: [
          {
            name: "ACTUAL",
            type: "column",
            data: data_output_actual,
          },
        ],
        output_options: {
          chart: {
            // height: 350,
            type: "line",
            toolbar: {
              show: false,
            },
          },
          title: {
            text: "Production",
            align: "center",
            style: {
              fontSize: "30px",
              fontWeight: "bold",
              color: "#0047ab",
            },
          },
          stroke: {
            width: 3,
            curve: "smooth",
          },
          markers: {
            size: 4,
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: "50%",
              dataLabels: {
                position: "center",
                orientation: "horizontal",
              },
            },
          },

          dataLabels: {
            enabled: true,
          },
          fill: {
            // opacity: [0.85, 0.25, 1],
            gradient: {
              inverseColors: false,
              shade: "light",
              type: "vertical",
              opacityFrom: 0.85,
              opacityTo: 0.55,
              stops: [0, 100, 100, 100],
            },
          },
          labels: label_output,
          xaxis: {
            type: "text",
            title: {
              text: "TIME (hour)",
              style: {
                fontSize: "15px",
              },
            },
          },
          yaxis: {
            title: {
              text: "OUTPUT (pcs)",
              style: {
                fontSize: "15px",
              },
            },
            min: 0,
            max: 100,
            labels: {
              show: true,
            },
          },
        },
      });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  output_daily = async () => {
    try {
      let result = await httpClient.post(server.OUTPUT_Day_URL);
      // console.log("line2", result.data.result);
      var label_output = [];
      var data_output_actual = [];
      for (let i = 0; i < result.data.result.length; i++) {
        label_output.push(result.data.result[i].mfgdate);
        data_output_actual.push(result.data.result[i].QTYS);
      }
      await this.setState({
        output_series_day: [
          {
            name: "ACTUAL",
            type: "column",
            data: data_output_actual,
          },
        ],
        output_options_day: {
          chart: {
            // height: 350,
            type: "line",
            toolbar: {
              show: false,
            },
          },
          stroke: {
            width: 3,
            curve: "smooth",
          },
          markers: {
            size: 4,
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: "50%",
              dataLabels: {
                position: "center",
                orientation: "horizontal",
              },
            },
          },

          dataLabels: {
            enabled: true,
          },
          fill: {
            gradient: {
              inverseColors: false,
              shade: "light",
              type: "vertical",
              opacityFrom: 0.85,
              opacityTo: 0.55,
              stops: [0, 100, 100, 100],
            },
          },
          labels: label_output,
          xaxis: {
            type: "datetime",
            title: {
              text: "Date",
              style: {
                fontSize: "15px",
              },
            },
          },
          yaxis: {
            title: {
              text: "OUTPUT (pcs)",
              style: {
                fontSize: "15px",
              },
            },
            min: 0,
            max: 100,
            labels: {
              show: true,
            },
          },
        },
      });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  stack_daily = async () => {
    try {
      console.log("check");
      let stack_data = await httpClient.post(server.SUB2_OUTPUT, {
        line: this.state.line,
        Customer: this.state.Customer,
      });
      var lable_data = [];
      let target = [];
      var data1 = [];
      var data2 = [];
      var data3 = [];
      for (let index = 0; index < stack_data.data.result.length; index++) {
        lable_data.push(stack_data.data.result[index].mfgdate);
        target.push(stack_data.data.result[index].target);
        data1.push(stack_data.data.result[index].PN_69005EW021);
        data2.push(stack_data.data.result[index].PN_69005EW031);
        data3.push(stack_data.data.result[index].PN_77310EW041);
      }
      this.setState({
        options_stack: {
          title: {
            text: "Production Daily Result",
            align: "center",
            style: {
              fontSize: "30px",
              fontWeight: "bold",
              color: "#0047ab",
            },
          },
          chart: {
            type: "line",
            stacked: true,
            toolbar: {
              show: true,
            },
            zoom: {
              enabled: true,
            },
          },
          dataLabels: {
            enabled: true,
          },
          stroke: {
            width: [1, 1, 4],
          },
          responsive: [
            {
              breakpoint: 100,
              options: {},
            },
          ],
          plotOptions: {
            bar: {
              horizontal: false,
              borderRadius: 5,
            },
          },
          colors: ["#F57C00", "#9FCD6A", "#57aeff", "#FF4560", "#E2C044"],
          xaxis: {
            type: "text",
            categories: lable_data,
            title: {
              text: "Date",
              style: {
                fontSize: "20px",
                align: "center",
              },
            },
          },
          yaxis: [
            {
              axisTicks: {
                show: true,
              },
              axisBorder: {
                show: true,
                color: "#008FFB",
              },
              labels: {
                style: {
                  colors: "#008FFB",
                },
              },
              title: {
                text: "Qty ",
                style: {
                  color: "#008FFB",
                },
              },
              tooltip: {
                enabled: true,
              },
              min: 0,
              max: 300,
              tickAmount: 20,
            },
          ],
          legend: {
            position: "bottom",
          },
          fill: {
            opacity: 1,
          },
        },
        series_stack: [
          {
            name: "PN_69005EW021",
            type: "column",
            data: data1,
          },
          {
            name: "PN_69005EW031",
            type: "column",
            data: data2,
          },
          {
            name: "PN_77310EW041",
            type: "column",
            data: data3,
          },
          {
            name: "Target",
            type: "line",
            data: target,
          },
        ],
      });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  stack_hour = async () => {
    try {
      console.log("check hour");
      let result = await httpClient.post(server.Hr_STACK, {
        mfg_date: this.state.mfg_date,
        line: this.state.line,
        Customer: this.state.Customer,
      });
      var label_hour = [];
      let target = [];
      var data21 = [];
      var data31 = [];
      var data41 = [];
      for (let k = 0; k < result.data.result.length; k++) {
        label_hour.push(result.data.result[k].hour);
        target.push(result.data.result[k].Target);
        data21.push(result.data.result[k].PN_69005EW021);
        data31.push(result.data.result[k].PN_69005EW031);
        data41.push(result.data.result[k].PN_77310EW041);
      }
      this.setState({
        options_hour: {
          title: {
            text: "Production Result by Hr",
            align: "center",
            style: {
              fontSize: "30px",
              fontWeight: "bold",
              color: "#0047ab",
            },
          },
          chart: {
            type: "line",
            stacked: true,
            toolbar: {
              show: true,
            },
            zoom: {
              enabled: true,
            },
          },
          dataLabels: {
            enabled: true,
          },
          stroke: {
            width: [1, 1, 4],
          },
          responsive: [
            {
              breakpoint: 100,
              options: {},
            },
          ],
          plotOptions: {
            bar: {
              horizontal: false,
              borderRadius: 5,
              hideZeroBarsWhenGrouped: true,
              dataLabels: {
                hideOverflowingLabels: true,
              },
            },
          },
          colors: ["#F57C00", "#9FCD6A", "#57aeff", "#FF4560", "#E2C044"],
          xaxis: {
            type: "text",
            categories: label_hour,
            title: {
              text: "Hr",
              style: {
                fontSize: "20px",
                align: "center",
              },
            },
          },
          yaxis: [
            {
              axisTicks: {
                show: true,
              },
              axisBorder: {
                show: true,
                color: "#008FFB",
              },
              labels: {
                style: {
                  colors: "#008FFB",
                },
              },
              title: {
                text: "Qty ",
                style: {
                  color: "#008FFB",
                },
              },
              tooltip: {
                enabled: true,
              },
              min: 0,
              max: 300,
              tickAmount: 20,
            },
          ],
          legend: {
            position: "bottom",
            showForZeroSeries: true,
          },
          fill: {
            opacity: 1,
          },
        },
        series_hour: [
          {
            name: "PN_69005EW021",
            type: "column",
            data: data21,
          },
          {
            name: "PN_69005EW031",
            type: "column",
            data: data31,
          },
          {
            name: "PN_77310EW041",
            type: "column",
            data: data41,
          },
          {
            name: "Target",
            type: "line",
            data: target,
          },
        ],
      });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  data_hour = async () => {
    try {
      let data = await httpClient.post(server.Hr_data, {
        mfg_date: this.state.mfg_date,
        line: this.state.line,
        Customer: this.state.Customer,
      });
      await this.setState({ datatable: data.data.result });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  data_daily = async () => {
    try {
      let Day_data = await httpClient.post(server.Daily_data, {
        line: this.state.line,
        Customer: this.state.Customer,
      });
      await this.setState({ datatable1: Day_data.data.result });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };
  renderTable() {
    if (this.state.datatable != null) {
      return this.state.datatable.map((item) => (
        <tr>
          <th>{item.mfgdate}</th>
          <th>{item.CUSTOMER_PN}</th>
          <th>{item.CUSTOMER_NAME}</th>
          <th>{item.MO_DATE}</th>
          <th>{item.DO_NUMBER}</th>
          <th>{item.ORDER_NO}</th>
          <th>{item.QTYS}</th>
        </tr>
      ));
    }
  }
  renderTable_daily() {
    if (this.state.datatable1 != null) {
      return this.state.datatable1.map((item) => (
        <tr>
          <th>{item.mfgdate}</th>
          <th>{item.CUSTOMER_PN}</th>
          <th>{item.CUSTOMER_NAME}</th>
          <th>{item.MO_DATE}</th>
          <th>{item.DO_NUMBER}</th>
          <th>{item.ORDER_NO}</th>
          <th>{item.QTYS}</th>
        </tr>
      ));
    }
  }
  monthly_data = async () => {
    try {
      let monthly = await httpClient.post(server.byMonth);
      console.log("hello", monthly.data.result);
      var month = [];
      var PN_69005EW021 = [];
      var PN_69005EW031 = [];
      var PN_77310EW041 = [];
      for (let x = 0; x < monthly.data.result.length; x++) {
        month.push(monthly.data.result[x].month);
        PN_69005EW021.push(monthly.data.result[x].PN_69005EW021);
        PN_69005EW031.push(monthly.data.result[x].PN_69005EW031);
        PN_77310EW041.push(monthly.data.result[x].PN_77310EW041);
      }
      await this.setState({
        seriesMonth: [
          {
            name: "PN_69005EW021",
            data: PN_69005EW021,
          },
          {
            name: "PN_69005EW031",
            data: PN_69005EW031,
          },
          {
            name: "PN_77310EW041",
            data: PN_77310EW041,
          },
        ],
        optionsMonth: {
          chart: {
            type: "bar",
            height: 350,
          },
          title: {
            text: "Monthly Production output",
            align: "center",
            style: {
              fontSize: "30px",
              fontWeight: "bold",
              color: "#0047ab",
            },
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: "55%",
              endingShape: "rounded",
              dataLabels: {
                position: "top",
              },
            },
          },
          colors: ["#F57C00", "#9FCD6A", "#57aeff", "#FF4560", "#E2C044"],
          dataLabels: {
            enabled: true,
          },
          stroke: {
            show: true,
            width: 2,
            colors: ["transparent"],
          },
          xaxis: {
            categories: month,
          },
          yaxis: {
            axisTicks: {
              show: true,
            },
            axisBorder: {
              show: true,
              color: "#008FFB",
            },
            labels: {
              style: {
                colors: "#008FFB",
              },
            },
            title: {
              text: "Qty ",
              style: {
                color: "#008FFB",
              },
            },
            tooltip: {
              enabled: true,
            },
            min: 0,
            max: 300,
            tickAmount: 20,
          },
          fill: {
            opacity: 1,
          },
          legend: {
            position: "bottom",
          },
        },
      });
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };

  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div
            className="container-fluid"
            style={{ fontFamily: "sans-serif", textAlign: "center" }}
          >
            <div className="card">
              <div className="col-md-12">
                <h1> SUB 2 PRODUCTION OUTPUT</h1>
                <div className="row">
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-2"></div>
                      <div className="col-md-2">
                        <div className="card-body">
                          <div className="form-group">
                            <h4>
                              <i
                                className="fas fa-calendar-day"
                                style={{ color: "red" }}
                              >
                                &nbsp;
                              </i>{" "}
                              DATE{" "}
                            </h4>
                            <input
                              className="form-control"
                              type="date"
                              id="id_daydate"
                              name="name_daydate"
                              value={this.state.mfg_date}
                              onChange={async (e) => {
                                await this.setState({
                                  mfg_date: moment(e.target.value).format(
                                    "YYYY-MM-DD"
                                  ),
                                });
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-md-2">
                        <div className="card-body">
                          <div className="form-group">
                            <h4>
                              {" "}
                              <i
                                className="fa fa-memory"
                                style={{ color: "green" }}
                              >
                                &nbsp;
                              </i>{" "}
                              LINE{" "}
                            </h4>
                            <select
                              value={this.state.line}
                              className="form-control"
                              onChange={async (e) => {
                                await this.setState({ line: e.target.value });
                              }}
                            >
                              <option> All </option>
                              <option> SUB2_L01 </option>
                              <option> SUB2_L02 </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-2">
                        <div className="card-body">
                          <div className="form-group">
                            <h4>
                              {" "}
                              <i
                                className="fa fa-user"
                                style={{ color: "purple" }}
                              >
                                &nbsp;
                              </i>{" "}
                              Customer PN{" "}
                            </h4>
                            <select
                              value={this.state.Customer}
                              className="form-control"
                              onChange={async (e) => {
                                await this.setState({
                                  Customer: e.target.value,
                                });
                              }}
                            >
                              <option> All </option>
                              <option> 69005EW021 </option>
                              <option> 69005EW031 </option>
                              <option> 77310EW041 </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-1">
                        <br />
                        <div className="card-body">
                          <div>
                            <button
                              type="submit"
                              className="btn btn-block btn-success"
                              onClick={(e) => {
                                e.preventDefault();
                                this.onClick_search();
                              }}
                            >
                              <i className="fas fa-search" /> &nbsp;
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <ReactApexChart
                          options={this.state.options_hour}
                          series={this.state.series_hour}
                          type="line"
                          height={600}
                        />
                      </div>
                      <div className="col-md-6">
                        <ReactApexChart
                          options={this.state.options_stack}
                          series={this.state.series_stack}
                          type="line"
                          height={600}
                        />
                      </div>
                    </div>
                    {/* <div className="row">
                      <div className="col-md-6">
                        <ReactApexChart
                          options={this.state.output_options}
                          series={this.state.output_series}
                          type="bar"
                          height={600}
                        />
                      </div>
                    </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">
                <i className="fas fa-file" /> &nbsp; Data Detail
              </h3>
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-plus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            <div className="card-body">
              <ul
                className="nav nav-tabs"
                id="custom-content-below-tab"
                role="tablist"
              >
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="custom-content-below-home-tab"
                    data-toggle="pill"
                    href="#custom-content-below-home"
                    role="tab"
                    aria-controls="custom-content-below-home"
                    aria-selected="true"
                  >
                    Data by Hour
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="custom-content-below-profile-tab"
                    data-toggle="pill"
                    href="#custom-content-below-profile"
                    role="tab"
                    aria-controls="custom-content-below-profile"
                    aria-selected="false"
                  >
                    Data by Daily
                  </a>
                </li>
              </ul>
              <div className="tab-content" id="custom-content-below-tabContent">
                <div
                  className="tab-pane fade"
                  id="custom-content-below-home"
                  role="tabpanel"
                  aria-labelledby="custom-content-below-home-tab"
                >
                  <div
                    className="card-body"
                    style={{ textAlign: "center", fontSize: "14px" }}
                  >
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>Mfg Date</th>
                          <th>CUSTOMER NUMBER</th>
                          <th>CUSTOMER NAME</th>
                          <th>MO NUMBER</th>
                          <th>DO NUMBER</th>
                          <th>ORDER NUMBER</th>
                          <th>QTy</th>
                        </tr>
                      </thead>
                      <tbody>{this.renderTable()}</tbody>
                    </table>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="custom-content-below-profile"
                  role="tabpanel"
                  aria-labelledby="custom-content-below-profile-tab"
                >
                  <div
                    className="card-body"
                    style={{ textAlign: "center", fontSize: "14px" }}
                  >
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>Mfg Date</th>
                          <th>CUSTOMER NUMBER</th>
                          <th>CUSTOMER NAME</th>
                          <th>MO NUMBER</th>
                          <th>DO NUMBER</th>
                          <th>ORDER NUMBER</th>
                          <th>QTy</th>
                        </tr>
                      </thead>
                      <tbody>{this.renderTable_daily()}</tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="card card-primary">
            <div className="card-header">
              {/* <h3 className="card-title">

                <i className="fas fa-file" /> &nbsp;
                Data Detail
              </h3> */}
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-plus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            <div className="card-body">
              <div className="col-md-12">
                <ReactApexChart
                  options={this.state.optionsMonth}
                  series={this.state.seriesMonth}
                  type="bar"
                  height={600}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SummarizeReport;
