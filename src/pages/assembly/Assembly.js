import React, { Component } from 'react'
import TagScanner from '../../components/scanparts/TagScanner';

class Assembly extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showComponent: false,
            activeButton: null
      }
    }
    showScannerClick = (scanner) =>{
        this.setState({ showComponent: true, activeButton: scanner });
    }


  render() {
    if (this.state.showComponent) {
        return <TagScanner text={this.state.activeButton} />;
    }
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="card">
              <div className="card-body text-center">
                <h2>Assembly : Select Tag</h2>
                <div className="row d-flex flex-column">
                  <div className="col">
                    <button className="btn btn-secondary btn-lg text-center mb-3 w-50"
                        onClick={() => this.showScannerClick('U-SHIN')}>
                      Tag U-SHIN
                    </button>
                  </div>
                  <div className="col">
                    <button className="btn btn-secondary btn-lg text-center mb-3 w-50"
                        onClick={() => this.showScannerClick('AAT')}>
                        Tag AAT
                    </button>
                  </div>
                  <div className="col">
                    <button className="btn btn-secondary btn-lg text-center mb-3 w-50"
                        onClick={() => this.showScannerClick('TEST')}>
                      Tag TEST
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Assembly;
