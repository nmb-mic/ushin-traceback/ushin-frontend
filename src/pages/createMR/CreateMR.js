import React, { Component } from "react";
import { httpClient } from "../../utils/HttpClient";
import { server } from "../../constants";
import ReactToPrint from "react-to-print";
import CreateQRCode from "./CreateQRCode";
import Swal from "sweetalert2";


class CreateMR extends Component {
  constructor(props) {
    super(props);

    this.state = {
      TTLQty: "",
      line_no: "",
      mo_no: "",
      cust_PN: "",
      process: "SUB2",
      lineTB: [],
      showRender: "none",
      qrCodesData: [],
      man_qty: "",
    };
  }

  PN_KeyPress = () => {
    this.inputPN.focus();
  };

  MO_KeyPress = () => {
    this.inputMO.focus();
  };

  getMO_qty = async () => {
    let resultMO_qty = await httpClient.post(server.GET_MO_QTY_URL, {
      MO_DATE: this.state.mo_no[0],
      process_line: this.state.process
    });
    try {
      var newResult = resultMO_qty.data.result[0];
      console.log(resultMO_qty)
      var moQty = Object.values(newResult)[2];
      console.log(moQty);
      this.setState({ man_qty: moQty });
      this.renderTableMR();
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }
  };

  TablePrintButton = () => {
    return (
      <ReactToPrint
        trigger={() => (
          <button className="btn btn-primary float-right">
            Print this out!
          </button>
        )}
        content={() => this.componentRef}
      />
    );
  };

  renderTableMR = async () => {
    let result = await httpClient.post(server.GET_PROCESS_LINE, {
      MO_DATE: this.state.mo_no[0],
      process_line: this.state.process
    });

    const resultList = result.data.result[0];
    console.log(resultList);
    resultList.forEach((item) => {
      item.QTYS =
        item.QTYS * Number(this.state.TTLQty) +
        item.QTYS * Number(this.state.TTLQty) * 0.01;
      
    });
    this.setState({ lineTB: resultList, showRender: "block" });
  };

  renderQrTable = () => {
    let qrTable = this.state.lineTB;
    if (qrTable.length > 0) {
      return (
        <div className="card card-info">
          <div className="card-header" style={{ fontSize: "30px" }}>
            <p>Material Requisition {this.TablePrintButton()} </p>
          </div>
          <div className="content">
            <CreateQRCode
              printData={qrTable}
              mo_no={this.state.mo_no[0]}
             // cust_PN={this.state.cust_PN}
              mo_qty={this.state.man_qty}
              ref={(el) => (this.componentRef = el)}
            />
          </div>
        </div>
      );
    }
  };

  render() {
    console.log(this.state.process);
    return (
      <>
        <div className="content-wrapper">
          <div className="content">
            <div className="container-fluid">
              <div className="card" style={{ height: "300px" }}>
                <div
                  className="card-header card-title text-bold"
                  style={{
                    fontSize: "45px",
                    textAlign: "center",
                    height: "100px",
                  }}
                >
                  <p> Create M/R </p>
                  <br />
                </div>
                {/* /.card-header */}
                <div className="card-body">
                  <div className="row">
                    <div className="col-3">
                      <div className="form-group" style={{ fontSize: "25px" }}>
                        <label>Process :</label>
                        <div className="input-group">
                          <select
                            style={{
                              height: "50px",
                              fontSize: "1.5rem",
                              fontWeight: "bold",
                            }}
                            className="form-control"
                            name="process"
                            onChange={(e) => {
                              this.PN_KeyPress();
                              this.setState({
                                process: e.target.value,
                              });
                            }}
                          >
                            <option
                              style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                              hidden
                              defaultValue="SUB2"
                            >
                              Select process
                            </option>
                            <option
                              style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                              value="MAIN"
                            >
                              MAIN
                            </option>
                            <option
                              style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                              value="SUB1"
                            >
                              SUB1
                            </option>
                            <option
                              style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                              Value="SUB2"
                              selected
                            >
                              SUB2
                            </option>
                          </select>
                        </div>
                      </div>
                    </div>

                    {/* <div className="col-3">
                      <div className="form-group" style={{ fontSize: "25px" }}>
                        <label>Customer P/N :</label>
                        <div>
                          <div className="input-group-prepend">
                            <input
                              style={{ height: "45px" }}
                              autoFocus
                              onKeyPress={this.MO_KeyPress}
                              ref={(input) => {
                                this.inputPN = input;
                              }}
                              className="form-control"
                              type="text"
                              onChange={(e) => {
                                this.setState({
                                  cust_PN: e.target.value,
                                });
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div> */}

                    <div className="col-3">
                      <div className="form-group" style={{ fontSize: "25px" }}>
                        <label>M/O No. :</label>
                        <div>
                          <div className="input-group-prepend">
                            <input
                              style={{ height: "45px" }}
                              className="form-control"
                              type="text"
                              ref={(input) => {
                                this.inputMO = input;
                              }}
                              onChange={(e) => {
                                try {
                                  const event = e.target.value
                                    .split(" ")
                                    .filter((part) => part !== "");

                                  if (event[0]["0"] === "M") {
                                    const eventNew = [event[0], event[1]];
                                    this.setState({ mo_no: eventNew });
                                  } else {
                                    Swal.fire({
                                      icon: "error",
                                      title: "Tag ไม่มีในระบบ",
                                      text: "Wrong tag",
                                      showConfirmButton: true,
                                      timer: 3000,
                                    });
                                    window.location.replace("../CreateMR");
                                  }
                                } catch (error) {
                                  return {
                                    result: "Failed",
                                    message: error.message,
                                  };
                                }
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-3">
                      <div className="form-group">
                        <br></br>
                        <div>
                          <div
                            style={{
                              width: "300px",
                              height: "65px",
                              fontSize: "35px",
                            }}
                            className="btn btn-primary"
                            type="submit"
                            onClick={async (e) => {
                              this.getMO_qty();
                            }}
                          >
                            <b>Create</b>
                          </div>
                        </div>
                      </div>
                    </div>

                    {/* <div className="col-3">
                          <div className="form-group">
                            <label>QTYs :</label>
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <input
                                  className="form-control"
                                  type="text"
                                  placeholder={this.state.man_qty}
                                />
                              </div>
                            </div>
                          </div>
                        </div> */}
                  </div>
                </div>
              </div>

              <div className="card" style={{ display: this.state.showRender }}>
                <div className="card-body">{this.renderQrTable()}</div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default CreateMR;
