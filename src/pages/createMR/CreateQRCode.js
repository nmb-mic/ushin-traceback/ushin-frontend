import React, { Component } from "react";
import QRCode from "qrcode.react";

class CreateQRCode extends Component {

  printData = this.props.printData;
 // customer_PN = this.props.cust_PN;
  MO_no = this.props.mo_no;
  MO_qty = this.props.mo_qty;



  render() {
  console.log(this.MO_qty)
  
    return (
      <div style={{ margin: "20px", padding: "50px" }}>
        <div className="content">
          <div className="container-fluid">
            <div className="card" style={{ boxShadow: "none" }}>
              <div className="row" style={{text:"center"}}>

              <div className="col-4">
                  {/* <h3>
                    Customer_PN. <u>{this.customer_PN}</u>{" "}
                  </h3> */}
                </div>
                <div className="col-4">
                  <h3>
                    M/O No. <u>{this.MO_no}</u>{" "}
                  </h3>
                </div>
                <div className="col-4">
                  <h3>
                    M/O QTY. <u>{this.MO_qty}</u>{" "}
                  </h3>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="table-responsive">
                <table style={{ border: "1px solid black" }}>
                  <thead>
                    <tr style={{ border: "2px solid black" }}>
                      <th style={{ border: "1px solid black" }}>PART_NAME</th>
                      <th style={{ border: "1px solid black" }}>PART_NO.</th>
                      <th style={{ border: "1px solid black" }}>PRCS_CODE</th>
                      <th style={{ border: "1px solid black" }}>PRCS_NAME</th>
                      <th style={{ border: "1px solid black" }}>
                        PROCESS_LINE
                      </th>
                      <th style={{ border: "1px solid black" }}>QRCODE</th>
                      <th style={{ border: "1px solid black" }}>QTY_PER_SET</th>
                      <th style={{ border: "1px solid black" }}>
                        QTYS_TTL_SET
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.printData.map((printItem, index) => (
                      <tr
                        key={index}
                        style={{
                          border: "2px solid black",
                          margin: "15px 0 30px 0",
                        }}
                      >
                        <td style={{ border: "1px solid black" }}>
                          {printItem.CHILD_PART_NAME}
                        </td>
                        <td style={{ border: "1px solid black" }}>
                          {printItem.CHILD_PART_NUMBER}
                        </td>
                        <td style={{ border: "1px solid black" }}>
                          {printItem.PROCESS_CODE}
                        </td>
                        <td style={{ border: "1px solid black" }}>
                          {printItem.PROCESS_NAME}
                        </td>
                        <td style={{ border: "1px solid black" }}>
                          {printItem.PROCESS_LINE}
                        </td>
                        <td style={{ border: "1px solid black" }}>
                          <div className="m-2">
                            <QRCode
                              size={100}
                              renderAs="svg"
                              //imageSettings={{ height: 7, width: 7, excavate: true }}
                              value={`${this.MO_no}\t${this.customer_PN}\t${
                                printItem.CHILD_PART_NAME
                              }\t${printItem.CHILD_PART_NUMBER}\t${
                                printItem.PROCESS_CODE
                              }\t${printItem.PROCESS_NAME}\t${
                                printItem.PROCESS_LINE
                              }\t${this.MO_qty}\t${
                                printItem.QTY_PER_SET
                              }\t${Math.ceil(
                                this.MO_qty / printItem.QTY_PER_SET
                              )}`}
                            />
                          </div>
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            textAlign: "center",
                          }}
                        >
                          {printItem.QTY_PER_SET}
                        </td>
                        <td
                          style={{
                            border: "1px solid black",
                            textAlign: "center",
                          }}
                        >
                          {Math.ceil(this.MO_qty / printItem.QTY_PER_SET)}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default CreateQRCode;
