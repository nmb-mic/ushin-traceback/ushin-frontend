import React from "react";

const Home = () => {
  // return const html Main element for use
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="row pt-4">
            <section className="content">
              <div className="container" style={{padding:"20px", marginLeft:"100px"}}>
              
                <img
                  src={require('./Ushin_homepage.png')}
                  alt="product"
                  width="130%"
                  height="200%"
                />
                {/* <path
                  fill="currentColor"
                  d="M288.662 352H31.338c-17.818 0-26.741-21.543-14.142-34.142l128.662-128.662c7.81-7.81 20.474-7.81 28.284 0l128.662 128.662c12.6 12.599 3.676 34.142-14.142 34.142z"
                /> */}
                <br />
              </div>
              <div></div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Home;
