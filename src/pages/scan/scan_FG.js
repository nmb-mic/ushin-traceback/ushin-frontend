import React, { Component } from "react";
import Swal from "sweetalert2";
import { server } from "../../constants/index";
import { httpClient } from "../../utils/HttpClient";
import moment from "moment";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";


class scan_FG extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MO_list: "",
      scanList: "",
      process: "SUB2",
      line: "",
      data: null,
      start_date: moment().format("YYYY-MM-DD"),
      end_date: moment().format("YYYY-MM-DD"),
      clear_state: [],
      time: this.props.time,
      seconds: "1200",
    };
  }

  insertMO = async () => {
    let insert_result = await httpClient.post(server.INSERT_FG_URL, {
      mo_date: this.state.MO_list[0],
      mo_no: this.state.MO_list[1],
      process: this.state.process,
      line: this.state.line,
      customer_PN: this.state.scanList[0],
      DO_number: this.state.scanList[1],
      card_no: this.state.scanList[2],
      due_date: this.state.scanList[3],
      order_no: this.state.scanList[4],
    });

    if (insert_result.data.api_result === "OK") {
      await Swal.fire({
        icon: "success",
        title: "บันทึกเรียบร้อย",
        text: "Record OK !!!",
        showConfirmButton: false,
        timer: 1000,
      });

      window.location.replace("../scan_FG");
    } else {
      await Swal.fire({
        icon: "error",
        title: "บันทึกซ้ำ",
        text: "Duplicate data",
        showConfirmButton: true,
      });
      window.location.replace("../scan_FG");
    }
  };

  line_KeyPress = () => {
    this.inputline.focus();
  };

  MO_KeyPress = async (event) => {
    console.log("mo", event.key);
    if (event.key === "Enter") {
      this.inputMO.focus();
    }
  };
  FG_KeyPress = async (event) => {
    console.log("fg", event.key);
    if (event.key === "Enter") {
      this.fg.focus();
    }
  };

  componentDidMount = async () => {
    this.getData();
    this.timer = setInterval(this.tick, 1000);
  };

  // getData = async () => {
  //   const result = await httpClient.get(server.GET_FG_BAL_URL);
  //   this.setState({ data: result.data.result });
  // };

  getData = async () => {
    try {
      let result = await httpClient.get(server.GET_FG_BAL_URL);
      this.setState({ data: result.data.result });

  
    } catch (error) {
      return {
        result: "Failed",
        message: error.message,
      };
    }

  };

  renderTableData() {
    if (this.state.data !== null) {
      console.log(this.state.data);
      return this.state.data.map((item) => (
        <tr>
          <td>{moment.utc(item.createdAt).format("DD-MM-YYYY")}</td>
          <td>{item.MO_DATE}</td>
          <td>{item.LINE}</td>
          <td>{item.CUSTOMER_PN}</td>
          <td>{item.QTY}</td>
        </tr>
      ));
    }
  }


  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="card" style={{ height: "220px" }}>
              <div
                className="card-header card-title text-bold"
                style={{
                  fontSize: "45px",
                  textAlign: "center",
                  height: "100px",
                }}
              >
                <p> Finished goods Process </p>
                <br />
              </div>
              <div className="card-body">
                <div className="row">
                  <div
                    className="col-2"
                    style={{
                      color: "#3495eb",
                      fontWeight: "bold",
                      textAlign: "center",
                      height: "50px",
                      fontSize: "25px",
                      width: "300px",
                    }}
                  >
                    <select
                      style={{
                        height: "50px",
                        fontSize: "1.5rem",
                        fontWeight: "bold",
                      }}
                      className="form-control"
                      name="process"
                      defaultValue={"SUB2"}
                      searchable={true}
                      onChange={(e) => {
                        this.line_KeyPress();
                        this.setState({
                          process: e.target.value,
                        });
                      }}
                    >
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        hidden
                        defaultValue="SUB2"
                      >
                        SUB2
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        value="MAIN"
                      >
                        MAIN
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        value="SUB1"
                      >
                        SUB1
                      </option>
                      <option
                        style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        Value="SUB2"
                        selected
                      >
                        SUB2
                      </option>
                    </select>
                  </div>
                  <div className="col-2">
                    <input
                      style={{
                        color: "#3495eb",
                        fontWeight: "bold",
                        textAlign: "center",
                        height: "50px",
                        fontSize: "25px",
                        width: "250px",
                      }}
                      autoFocus
                      ref={(input) => {
                        this.inputline = input;
                      }}
                      type="text"
                      className="form-control"
                      onKeyPress={this.MO_KeyPress}
                      placeholder="Scan Line"
                      onInput={async (e) => {
                        try {
                          await this.setState({ line: e.target.value });
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-2">
                    <input
                      style={{
                        color: "#3495eb",
                        height: "50px",
                        fontWeight: "bold",
                        fontSize: "25px",
                        textAlign: "center",
                        width: "250px",
                      }}
                      ref={(input) => {
                        this.inputMO = input;
                      }}
                      type="text"
                      className="form-control"
                      onKeyPress={this.FG_KeyPress}
                      placeholder="Scan M/O"
                      onInput={async (e) => {
                        try {
                          const event = e.target.value
                            .split(" ")
                            .filter((part) => part !== "");
                          console.log(event);

                          if (event[0]["0"] === "M") {
                            const eventNew = [event[0], event[1]];
                            await this.setState({ MO_list: eventNew });
                          } else {
                            await Swal.fire({
                              icon: "error",
                              title: "MO ไม่มีในระบบ",
                              text: "Wrong MO tag",
                              showConfirmButton: true,
                              timer: 3000,
                            });
                            window.location.replace("../kit_up");
                          }
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-4">
                    <input
                      style={{
                        color: "#3495eb",
                        fontWeight: "bold",
                        textAlign: "center",
                        fontSize: "25px",
                        width: "500px",
                        height: "50px",
                      }}
                      ref={(input) => {
                        this.fg = input;
                      }}
                      type="text"
                      class="form-control"
                      placeholder="Finished goods tag"
                      onInput={async (e) => {
                        try {
                          const event = e.target.value
                            .split(" ")
                            .filter((part) => part !== "");

                          if (event[0]["0"] === "S") {
                            const eventNew = [
                              event[0].substring(2),
                              event[2],
                              event[3].substring(5, 8),
                              event[3].substring(8, 16),
                              event[7],
                            ];
                            await this.setState({ scanList: eventNew });
                            console.log("test");
                          } else if (
                            (event[0]["0"] === "S") &
                            (event[1]["0"] !== "I")
                          ) {
                            const eventNew = [
                              event[0].substring(2),
                              event[1],
                              event[2].substring(5, 8),
                              event[2].substring(8, 16),
                              event[6],
                            ];
                            await this.setState({ scanList: eventNew });
                          } else {
                            await Swal.fire({
                              icon: "error",
                              title: "Tag ไม่มีในระบบ",
                              text: "Wrong tag",
                              showConfirmButton: true,
                            });
                            window.location.replace("../scan_FG");
                          }
                        } catch (error) {
                          return {
                            result: "Failed",
                            message: error.message,
                          };
                        }
                      }}
                    ></input>
                  </div>
                  <div className="col-1">
                    <button
                      style={{
                        marginBottom: "0",
                        fontSize: "1.5rem",
                        color: "white",
                        fontWeight: "bold",
                        width: "150px",
                      }}
                      className="btn btn-primary"
                      onClick={async (e) => {
                        e.preventDefault();
                        this.insertMO();
                        if (this.state.result === "OK") {
                          await Swal.fire({
                            icon: "success",
                            title: "บันทึกเรียบร้อย",
                            text: "Record OK !!!",
                            showConfirmButton: false,
                            timer: 1000,
                          });
                        }
                      }}
                    >
                      Add FG
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className="card"  style={{height:"900px"}}>
              <div className="row">
                <div className="table-responsive">
                  <MDBTable
                    responsive
                    style={{ textAlign: "center", fontSize: "2rem" ,width:"2220px",marginLeft:"20px",marginTop:"30px"}}
                  >
                    <MDBTableHead
                      color="primary-color"
                      style={{ backgroundColor: "#54B4D3" }}
                      textblack
                    >
                      <tr>
                        <th> REGIST_DATE</th>
                        <th> MO_DATE</th>
                        <th> LINE</th>
                        <th> CUSTOMER_PN</th>
                        <th> QTY</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>{this.renderTableData()}</MDBTableBody>
                  </MDBTable>
                </div>
              </div>
            </div>
        </div>
      </div>
      </div>
    );
  }
}
export default scan_FG;
