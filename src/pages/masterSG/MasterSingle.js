import React, { Component } from 'react'
import DataTable from 'react-data-table-component';
import { httpClient } from '../../utils/HttpClient';
import { server } from '../../constants';
// import QRCode from "react-qr-code";
// import Base64Image from '../../components/scanparts/ฺBase64Image';

const columns = [
  {
      name: 'No.',
      selector: row => String(row.ID),
      maxWidth: "30px",
      sortable: true,
  },
  {
      name: 'Child Part Name',
      selector: row => row.CHILD_PART_NAME,
  },
  {
    name: 'Child Part Code',
    selector: row => String(row.CHILD_PART_CODE),
    sortable: true,
  },
  {
    name: 'Process Name',
    selector: row => String(row.PROCESS_NAME),
    sortable: true,
  },
  {
    name: 'Code',
    selector: row => String(row.PROCESS_CODE),
    maxWidth: "30px",
    sortable: true,
  },
  {
    name: 'Edit',
    selector: row => <i className="fas fa-pencil-alt" onClick={()=> alert(String(row.ID))}></i>,
    maxWidth: "40px",
    style: row => ({paddingLeft: "25px"})
  },
  {
      name: 'Delete',
      selector: row => <i className="fas fa-trash" onClick={()=> alert(String(row.CHILD_PART_CODE))}></i>,
      maxWidth: "40px",
      style: row => ({paddingLeft: "25px"})
  },   
]


const customSyles = {
  headCells: {
    style: {
        texAlign: 'left',
        fontSize: '1rem',
        fontWeight: 'bold',
        backgroundColor: 'rgba(68,68,68,0.1)'
    },
  },
}

// const qr = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAAAklEQVR4AewaftIAAAOKSURBVO3BQY5bCRYDweSD7n/lHC96wdUHBKlsdw8j4i/M/OOYKcdMOWbKMVOOmXLMlGOmHDPlmCnHTDlmyjFTjplyzJRjprz4UBJ+J5UnSWgqT5LQVFoSmkpLQlNpSfidVD5xzJRjphwz5cWXqXxTEt6h0pLwjiQ0lZaET6h8UxK+6Zgpx0w5ZsqLH5aEd6i8IwlN5YnK3yQJ71D5ScdMOWbKMVNe/Mckoam0JHyTyn/JMVOOmXLMlBf/MSpPVFoSmso7ktBU/s2OmXLMlGOmvPhhKr9TEppKS8InktBUPqHyNzlmyjFTjpny4suS8CeptCQ0lZaEJ0loKi0JTeVJEv5mx0w5ZsoxU158SOVvkoS/icq/yTFTjplyzJT4Cx9IQlNpSfgmlSdJeIdKS8ITlXck4ZtUftIxU46ZcsyUFz9MpSWhqbwjCe9QaUloSWgqLQlPktBUmkpLQlNpSWgqLQlN5ZuOmXLMlGOmvPiyJLwjCU2lJaGptCQ0lXeovCMJ70hCU/mmJDSVTxwz5Zgpx0yJv/CDktBUWhKeqHxTEprKO5LwTSqfSEJT+cQxU46ZcsyUF3+YypMkvEPliUpLwidUWhKeqLQkvEOlqXzTMVOOmXLMlPgLH0hCU2lJeKLSktBUWhKaSkvCE5VvSsITlZaEpvIkCU9UvumYKcdMOWbKi79cEprKE5UnSXii0pLwiSS8IwlNpSWhJaGpfOKYKcdMOWbKix+m0pLQkvBEpSWhqXxC5R0qLQlNpSWhqbQkNJU/6Zgpx0w5ZsqL30ylJaGptCQ0lZaEptKS8AmVloQnSXiShCdJ+JOOmXLMlGOmvPiQyidUnqh8QuVJEp6ovEPlHUl4ovI7HTPlmCnHTHnxoST8TipN5UkSmkpTaUloSWgqLQlPktBUnqg8ScITlU8cM+WYKcdMefFlKt+UhCdJ+EQSnqh8QuXf5Jgpx0w5ZsqLH5aEd6h8QqUl4R0qLQnvSMI3JeF3OmbKMVOOmfLiPyYJTeWJyhOVdyThico7VFoSftIxU46ZcsyUF/9nkvBE5UkSnqh8IglNpam0JHzTMVOOmXLMlBc/TOUnqbQkvEPlSRKeqDxJwhOVpvInHTPlmCnHTHnxZUn4nZLwk1RaEp4k4RNJ+JOOmXLMlGOmxF+Y+ccxU46ZcsyUY6YcM+WYKcdMOWbKMVOOmXLMlGOmHDPlmCnHTPkfk+GMGSD5nAwAAAAASUVORK5CYII="
// const imageSize = { width: 150, height: 150 };

// const data = 'Hello\tWorld!\tWorld!2\tWorld!3\tWorld!4\tWorld!5\t';

class MasterSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      h1: "",
      h2: "",
      h3: "",
      h4: "",
      h5: "",
      h6: "",
      listMasterSG : [],
      qr_gen : ''
    }
  }

  componentDidMount(){
    this.renderListMasterSG()
  }

  renderListMasterSG = async () => {
    let result = await httpClient.get(server.ALL_MASTER_SG);
    this.setState({listMasterSG : result.data.result})
  }

  // renderMasterQR = async () => {
  //   let result = await httpClient.get(server.GENERATE_QR);
  //   console.log(result.data.qrCodeImg);
  //   this.setState({qr_gen : result.data.qrCodeImg})
  // }

  splitData = (data) => {
    const lines = data.split(' ');
    const shiftedLines = lines.map((line, index) => `${index + 1}. ${line}`);
    return shiftedLines.join('\n');
  };

  // openPrintTab = () => {
  //   const qrCodeData = encodeURIComponent(data);
  //   const url = `
  //     <html>
  //       <body>
  //         <div class="container" style="text-align: center">
  //           <div>
  //             <p style="font-size: 20px">create new qr code</p>
  //           </div>
  //           <img src="https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=${qrCodeData}" alt="QR Code" />
  //         </div>
  //       </body>
  //     </html>
  //   `;

  //   const newTab = window.open();
  //   newTab.document.write(url);
  //   newTab.document.close();
  // }

  render() {
    return (
      <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="card">
            <div className="card-body text-center">
              <h2>Add Master Single Part</h2>
              <div className="row">
                <div className='col-2'></div>

                <div className="col-4 d-flex flex-column">
                  <label>Single Part Number 1</label>
                  <input type="text" onInput={e => this.setState({h1: e.target.value})}/>
                </div>
                <div className="col-4 d-flex flex-column">
                <label>Single Part Name</label>
                  <input type="text" onInput={e => this.setState({h2: e.target.value})}/>
                </div>
                <div className='col-2'></div>
              </div>
              <div className="row">
                <div className='col-2'></div>

                <div className="col-4 d-flex flex-column">
                  <label>Single Part Number 2</label>
                  <input type="text"/>
                </div>
                <div className="col-4 d-flex flex-column">
                <label>Single Part Name</label>
                  <input type="text"/>
                </div>
                <div className='col-2'></div>
              </div>
              <div className="row">
                <div className='col-2'></div>

                <div className="col-4 d-flex flex-column">
                  <label>Single Part Number 3</label>
                  <input type="text"/>
                </div>
                <div className="col-4 d-flex flex-column">
                <label>Single Part Name</label>
                  <input type="text"/>
                </div>
                <div className='col-2'></div>
              </div>
              <div className="row">
                <div className='col-2'></div>

                <div className="col-4 d-flex flex-column">
                  <label>Single Part Number 3</label>
                  <input type="text"/>
                </div>
                <div className="col-4 d-flex flex-column">
                <label>Single Part Name</label>
                  <input type="text"/>
                </div>
                <div className='col-2'></div>
              </div>

              <div className="row">
                <div className='col-2'></div>

                <div className="col-4 d-flex flex-column">
                  <label>Single Part Number 3</label>
                  <input type="text"/>
                </div>
                <div className="col-4 d-flex flex-column">
                <label>Single Part Name</label>
                  <input type="text"/>
                </div>
                <div className='col-2'></div>
              </div>
              
              <div className='row'>
                <div className='col-12 mt-2'>
                  <button className='btn btn-primary' onClick={()=>alert("ok")}>Add Part</button>
                  <button className='btn btn-secondary' onClick={this.openPrintTab}>Open QR Code in New Tab</button>
                </div>
              </div>
            </div>
            <div className='card-body'>
              <DataTable
                columns={columns}
                data={this.state.listMasterSG}
                customStyles={customSyles}
                dense
                pagination
              />
            </div>

            {/* <div className='card-body'>
              <QRCode
                size={150}
                bgColor='white'
                fgColor='black'
                value={data}
              />
            </div>
            <div className='card-body'>
                <Base64Image base64Image={this.state.qr_gen} size={imageSize}/>
            </div> */}
          </div>
        </div>
      </div>
    </div>
    )
  }
}
export default MasterSingle;