import React, { Component } from "react";
import DataTable from 'react-data-table-component';
import { httpClient } from "../../utils/HttpClient";
import { server } from "../../constants";

const columns = [
  {
      name: 'M/O',
      selector: row => String(row.MO_NUMBER),
      maxWidth: "15px",
      sortable: true,
  },
  // {
  //     name: 'Customer',
  //     selector: row => row.CUSTOMER_NUMBER,
  // },
  {
    name: 'Prcs Code',
    selector: row => String(row.PROCESS_CODE),
    sortable: true,
  },
  {
    name: 'Prcs Name',
    selector: row => String(row.PROCESS_NAME),
    sortable: true,
  },
  // {
  //   name: 'Part No.',
  //   selector: row => String(row.CHILD_PART_NUMBER),
  //   sortable: true,
  // },
  {
    name: 'Part Name',
    selector: row => String(row.CHILD_PART_NAME),
    sortable: true,
  },
  {
    name: 'Qty/Box',
    selector: row => String(row.QTY_PER_BOX),
    // maxWidth: "25px",
    sortable: true,
  },
  {
    name: 'Qty.',
    selector: row => String(row.QTYS),
    maxWidth: "10px",
    sortable: true,
  },
  {
    name: 'Edit',
    selector: row => <i className="fas fa-pencil-alt" onClick={()=> alert(String(row.TRANSACTION_ID))}></i>,
    maxWidth: "10px",
    // style: row => ({paddingLeft: "25px"})
  },
  {
      name: 'Delete',
      selector: row => <i className="fas fa-trash" onClick={()=> alert(String(row.TRANSACTION_ID))}></i>,
      maxWidth: "10px",
      // style: row => ({paddingLeft: "25px"})
  },   
]

const customSyles = {
  headCells: {
    style: {
        texAlign: 'left',
        fontSize: '1rem',
        fontWeight: 'bold',
        backgroundColor: 'rgba(68,68,68,0.1)'
    },
  },
}

class DemoScanner extends Component {

  constructor(props){
    super(props);

    this.inputRef = React.createRef();
    this.inputRef2 = React.createRef();
    this.inputRef3 = React.createRef();
    this.inputRef4 = React.createRef();
    this.inputRef5 = React.createRef();
    this.inputRef6 = React.createRef();
    this.inputRef7 = React.createRef();

    this.state = {
      mo_no: "",
      cus_no: "",
      process_code: "",
      part_no: "",
      part_name: "",
      qty_per_box: 0,
      qty: 0,
      mo_table: []
    }
  }

  componentDidMount(){
    this.renderTransactionTable();
  }

  initialState = () => {
    this.inputRef.current.value = '';
    this.inputRef2.current.value = ''; 
    this.inputRef3.current.value = ''; 
    this.inputRef4.current.value = ''; 
    this.inputRef5.current.value = ''; 
    this.inputRef6.current.value = ''; 
    this.inputRef7.current.value = ''; 
    this.setState({
      mo_no: "",
      cus_no: "",
      process_code: "",
      part_no: "",
      part_name: "",
      qty_per_box: 0,
      qty: 0,
    })
    this.inputRef.current.focus();
  }

  insertMO = async () => {

    // let Chk = await httpClient.get(server.ALL_TRANS_MO)
    await httpClient.post(server.INSERT_MO_PART, {
      mo_no: this.state.mo_no, 
      cus_no: this.state.cus_no, 
      process_code: this.state.process_code, 
      part_no: this.state.part_no, 
      part_name: this.state.part_name, 
      qty_per_box: this.state.qty_per_box, 
      qty: this.state.qty
    });
      this.renderTransactionTable();
  }

  renderTransactionTable = async() => {
    let result = await httpClient.get(server.ALL_TRANS_MO);
    // console.log(result.data);
    this.setState({mo_table : result.data.result})
    this.initialState();
  }
  render() {
    return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="card">
              <div className="card-body text-center">
                <div className="row">

                  <div className="col-4 d-flex flex-column">
                    <label className="mb-0">Manufacturing Order No.</label>
                    <input type="text" ref={this.inputRef}
                      onInput={e => this.setState({mo_no : e.target.value})}/>
                  </div>
                  <div className="col-4 d-flex flex-column">
                    <label className="mb-0">Customer No.</label>
                    <input type="text" ref={this.inputRef2}
                      onInput={e => this.setState({cus_no : e.target.value})}/>
                  </div>
                  <div className="col-4 d-flex flex-column">
                    <label className="mb-0">Process Code</label>
                    <input type="text" ref={this.inputRef3}
                      onInput={e => this.setState({process_code : e.target.value})}/>
                  </div>
                </div>

                <div className="row pt-3">

                  <div className="col-3 d-flex flex-column">
                    <label className="mb-0">Part Number</label>
                    <input type="text" ref={this.inputRef4}
                      onInput={e => this.setState({part_no : e.target.value})}/>
                  </div>
                  <div className="col-3 d-flex flex-column">
                    <label className="mb-0">Part Name</label>
                    <input type="text" ref={this.inputRef5}
                      onInput={e => this.setState({part_name : e.target.value})}/>
                  </div>
                  <div className="col-3 d-flex flex-column">
                    <label className="mb-0">Qty/Box</label>
                    <input type="text" ref={this.inputRef6}
                      onInput={e => this.setState({qty_per_box : e.target.value})}/>
                  </div>
                  <div className="col-3 d-flex flex-column">
                    <label className="mb-0">Qty</label>
                    <input type="text" ref={this.inputRef7}
                      onInput={e => this.setState({qty : e.target.value})}/>
                  </div>
                </div>

                <div className='row'>
                <div className='col-12 mt-2'>
                  <button className='btn btn-primary' onClick={this.insertMO}>Add Part</button>
                </div>
              </div>
              </div>
            </div>

            <div className="card">
            <div className='card-body'>
              <DataTable
                columns={columns}
                data={this.state.mo_table}
                customStyles={customSyles}
                dense
                pagination
              />
            </div>
            </div>

            
          </div>
        </div>
      </div>
    );
  }
}
export default DemoScanner;




// import React, { useRef, useState, useEffect } from "react";
// import DataTable from 'react-data-table-component';
// import { httpClient } from "../../utils/HttpClient";
// import { server } from "../../constants";

// const columns = [
//   {
//     name: 'M/O',
//     selector: row => String(row.MO_NUMBER),
//     maxWidth: "15px",
//     sortable: true,
//   },
//   {
//     name: 'Prcs Code',
//     selector: row => String(row.PROCESS_CODE),
//     sortable: true,
//   },
//   {
//     name: 'Prcs Name',
//     selector: row => String(row.PROCESS_NAME),
//     sortable: true,
//   },
//   {
//     name: 'Part Name',
//     selector: row => String(row.CHILD_PART_NAME),
//     sortable: true,
//   },
//   {
//     name: 'Qty/Box',
//     selector: row => String(row.QTY_PER_BOX),
//     sortable: true,
//   },
//   {
//     name: 'Qty.',
//     selector: row => String(row.QTYS),
//     maxWidth: "10px",
//     sortable: true,
//   },
//   {
//     name: 'Edit',
//     selector: row => <i className="fas fa-pencil-alt" onClick={() => alert(String(row.TRANSACTION_ID))}></i>,
//     maxWidth: "10px",
//   },
//   {
//     name: 'Delete',
//     selector: row => <i className="fas fa-trash" onClick={() => alert(String(row.TRANSACTION_ID))}></i>,
//     maxWidth: "10px",
//   },   
// ]

// const customStyles = {
//   headCells: {
//     style: {
//       textAlign: 'left',
//       fontSize: '1rem',
//       fontWeight: 'bold',
//       backgroundColor: 'rgba(68,68,68,0.1)'
//     },
//   },
// }

// const DemoScanner = () => {
//   const inputRef = useRef();
//   const inputRef2 = useRef();
//   const inputRef3 = useRef();
//   const inputRef4 = useRef();
//   const inputRef5 = useRef();
//   const inputRef6 = useRef();
//   const inputRef7 = useRef();

//   const [mo_no, setMoNo] = useState("");
//   const [cus_no, setCusNo] = useState("");
//   const [process_code, setProcessCode] = useState("");
//   const [part_no, setPartNo] = useState("");
//   const [part_name, setPartName] = useState("");
//   const [qty_per_box, setQtyPerBox] = useState(0);
//   const [qty, setQty] = useState(0);
//   const [mo_table, setMoTable] = useState([]);

//   useEffect(() => {
//     renderTransactionTable();
//   }, []);

//   const initialState = () => {
//     inputRef.current.value = '';
//     inputRef2.current.value = ''; 
//     inputRef3.current.value = ''; 
//     inputRef4.current.value = ''; 
//     inputRef5.current.value = ''; 
//     inputRef6.current.value = ''; 
//     inputRef7.current.value = ''; 

//     inputRef.current.focus();
//   }

//   const insertMO = async () => {
//     await httpClient.post(server.INSERT_MO_PART, {
//       mo_no, cus_no, process_code, part_no, part_name, qty_per_box, qty
//     });
//     renderTransactionTable();
//   }

//   const renderTransactionTable = async () => {
//     let result = await httpClient.get(server.ALL_TRANS_MO);
//     setMoTable(result.data.result);
//     initialState();
//   }

//   return (
//     <div className="content-wrapper">
//       <div className="content">
//         <div className="container-fluid">
//           <div className="card">
//             <div className="card-body text-center">
//               <div className="row">
//                 <div className="col-4 d-flex flex-column">
//                   <label className="mb-0">Manufacturing Order No.</label>
//                   <input type="text" ref={inputRef} onInput={e => setMoNo(e.target.value)} />
//                 </div>
//                 <div className="col-4 d-flex flex-column">
//                   <label className="mb-0">Customer No.</label>
//                   <input type="text" ref={inputRef2} onInput={e => setCusNo(e.target.value)} />
//                 </div>
//                 <div className="col-4 d-flex flex-column">
//                   <label className="mb-0">Process Code</label>
//                   <input type="text" ref={inputRef3} onInput={e => setProcessCode(e.target.value)} />
//                 </div>
//               </div>

//               <div className="row pt-3">
//                 <div className="col-3 d-flex flex-column">
//                   <label className="mb-0">Part Number</label>
//                   <input type="text" ref={inputRef4} onInput={e => setPartNo(e.target.value)} />
//                 </div>
//                 <div className="col-3 d-flex flex-column">
//                   <label className="mb-0">Part Name</label>
//                   <input type="text" ref={inputRef5} onInput={e => setPartName(e.target.value)} />
//                 </div>
//                 <div className="col-3 d-flex flex-column">
//                   <label className="mb-0">Qty/Box</label>
//                   <input type="text" ref={inputRef6} onInput={e => setQtyPerBox(e.target.value)} />
//                 </div>
//                 <div className="col-3 d-flex flex-column">
//                   <label className="mb-0">Qty</label>
//                   <input type="text" ref={inputRef7} onInput={e => setQty(e.target.value)} />
//                 </div>
//               </div>

//               <div className='row'>
//                 <div className='col-12 mt-2'>
//                   <button className='btn btn-primary' onClick={insertMO}>Add Part</button>
//                 </div>
//               </div>
//             </div>
//           </div>

//           <div className="card">
//             <div className='card-body'>
//               <DataTable
//                 columns={columns}
//                 data={mo_table}
//                 customStyles={customStyles}
//                 dense
//                 pagination
//               />
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default DemoScanner;

